import gc
import sys
sys.path.insert(0,'../')
import numpy as np
import random
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Activation, GlobalAveragePooling1D,GlobalMaxPooling1D,Conv1D,MaxPooling1D, LeakyReLU, Dropout
from keras import regularizers
from keras import optimizers
from keras.callbacks import TensorBoard
from tools import functions as f

# 'D64486QJNU3N'

#bad users:
bad_users = ['5L2EU7R47RPH', '3780JL14EV1D', 'FO8QPTSQ5U70', '4L4FBJHFUJRH', 'YN5MTCF9GP4B',
             'PY15G570UF8D','8CLJKC9IFWL8', 'IOO7FBHHDQU8','VIQBP8LSGKGC','JXVAT95P4XBT',
             'FQCF1SROJ24G', 'W4NO5RE4V34Y','GGL24VV818FR','T3YZ7TSF170R','TXVP014W94PC',
             'JKCGNE0IVGP9', '22FAQPQPWU6P','R0EAE2U37U94', '3G3BH50P6C03','0MAQUFEBRUKR',
             'EU5HJBCAXQ64', 'WFGFH6RKASZ7', '16YVO2K5D74I', 'LWGCPH3S57DZ', 'HZHGDWEX09NJ',
             '46062VC85TRD','48J3KURRVZND','SRI29R9TLZPU','KAAU00HNDW1L','OQW9WYMFCP9E',
             'PRKG46XX1B65','73V0H02145H1','CAQGJQ8Z46DD','K4FGCD831EZA','8BW39U6R98ER',
             '0CFFUM9FB70I', '18NZPVIUF46N','C5AY2Y3KME67','1R3OOFG1ON90', 'TEL4278EULLC'
             ,'YVOERAFO44BL', 'R0CXU3D7LPVG' , '1BBQCSTMIZ5K', 'L747FTDBPNY5', 'HTQV53QD6AI5',
             '638KDQ4Y5Y1P', 'EO7GDMH66DI7','NJ150G4ZK3N9','0ZLBNXELU140','5WC4DA9A4FYC',
             'WPBA01AAA9RH', '6Y69KEV6Y7OP','QNFFH4YWTLFG', '3QPZRADPIBVS', 'FCF1S9SE57LW'
             ,'600IKHG3Z950','86S6D60H8QTG', 'OHQU65XB8XIC','HDSBFA8160BY', 'XP24MP03NMNK'
             , 'HQD84A6Q4WP2', '7CLADA3PGX65', 'M3S0V07W92NA','BJI06GQ1NIRC', 'A4P0XT93U1VF'
             ,'DYMMH89YQUOO', 'M579NU0104R1','YECCZHDLDMTE', '0Y096J43UOS9', 'VBXYVE06LIRL',
             'A1O8IL7XS1F9', '1NKX9L3KSLVN','MC664I5YHG7S','04V3J14J7BGX','STHITNQXO7VK',
             'A5FF8WPD9Z8J', '9FHDMC26C0TB', 'C7ULW8I933EA','9D3M31MKMIKX', 'YC2YBZ9SCJ2R'
             , 'W200RN3VYH9L', 'SLO6IMS0G9CR', '6D23ZC9OIP98','U5KIQ6XB1ILU', '8QXE38TC9JXM',
             '3893GC9WJWZB'
]
vectorizer = f.create_vectorizer()
train_data = f.df_from_csv("../../data/train.csv")
mask = train_data.action_type.isin(['interaction item image'
                                    ,'interaction item info'
                                    ,'interaction item rating'
                                    ,'interaction item deals'
                                    ,'filter selection'
                                    ,'clickout item'])
train_data = train_data[mask]

bad_user_mask = train_data.user_id.isin(bad_users)
train_data = train_data[~bad_user_mask]

gc.collect()

metadata = f.df_from_csv("../../data/item_metadata.csv")
user_ids = train_data.user_id.unique()

vectorizer.fit_transform(metadata.properties)

feature_names = vectorizer.get_feature_names()

def fetch_sessions(user_ids):
  df = pd.DataFrame(columns=['label','observation'])
  for i in user_ids:
    session =  train_data.loc[train_data.user_id == i]
    observation_array = []
    #print(i)
    pdf = session.loc[session.action_type == 'clickout item'].prices.values
    idf = session.loc[session.action_type == 'clickout item'].impressions.values
    prices = np.hstack(list(map(lambda x: x.split("|"),pdf))).astype(float)
    imps = np.hstack(list(map(lambda x: x.split("|"),idf)))
    imp_prices = dict(zip(imps,prices))
    for s in session.index:
      label = np.full(157,0.1)
      current = session.loc[s]
      if current.action_type == "clickout item":
        #create label for this session
        label = f.create_observation(f.get_item_properties(current.reference,metadata),feature_names)
        #label = np.ones(157)
        impressions = f.get_impressions(current)
        for imp in impressions:
          arr= f.create_observation(f.get_item_properties(imp,metadata),feature_names)
          price = imp_prices.get(imp)
          if price:
            arr *= price
          observation_array.append(arr)
        #get last 100 actions if there are some
        last40 = observation_array[-40:]
        #is it less than 100 elements fill up
        if len(last40)< 40:
          tmp = np.full((40,157),0.1)
          offset = 40 - len(last40)
          first = last40[0]
          for i in range(0,offset):
            tmp[i] = first

          last40.reverse()
          for i in range(offset,40):
            tmp[i] = last40.pop()
          last40=tmp
          observation_array = np.array(last40)
        else:
         observation_array = np.array(observation_array)

        observation_array = np.reshape(observation_array,(1,observation_array.shape[0],observation_array.shape[1]))
        #observation_array = np.expand_dims(observation_array,axis=2)
        df1 = pd.DataFrame([[np.array(label),observation_array]]
                             ,columns=['label','observation'])
        #df= pd.concat([df1,df])
        df = df.append(df1)
        df1 = []
        label= []
        impressions = []
        observation_array = []
        gc.collect()
      else:
        arr = np.full(157,0.1)
        #accumulate session obs
        if current.action_type == "filter selection":
          arr = f.create_observation(f.filter_selections_to_array(current.current_filters),feature_names)


        else:
          arr = f.create_observation(f.get_item_properties(current.reference,metadata),feature_names)

          price = imp_prices.get(current.reference)
          if price:
            arr *= price
          observation_array.append(arr)
  return df


def create_batch(batch_size:int=1):
  random_users = []
  label_batch = []
  for i in range(batch_size):
    user_id = user_ids[random.randint(0,len(user_ids)-1)]
    #if no clickout events present rechoose
    while not (train_data.loc[train_data.user_id == user_id].action_type == 'clickout item').any():
      print("bad user: ", user_id)
      user_id = user_ids[random.randint(0,len(user_ids)-1)]
    random_users.append (user_id)
   #print("user was", random_users)
  df = fetch_sessions(random_users)
  label_batch = df.get('label').values
  session_batch = df.drop('label',axis=1)
  session_batch = session_batch.get('observation').values

  #return session_batch[0], np.reshape(label_batch[0],(1,157))
  return session_batch, label_batch

#def batch_generator(batch_size: int):
#  while True:
    #get slices
#    for i in range(batch_size):
#      session, label = create_batch()
#      yield session, label


def batch_generator(batch_size: int):
  while True:
    sessions, labels= create_batch(batch_size)
    for i in range(batch_size):

      yield sessions[i], np.reshape(labels[i],(1,157))


def conv1d_model(input_width: int):
  model = Sequential()
  model.add(Conv1D(filters=64,kernel_size=5,
                   input_shape=(None,input_width),
                   padding="valid",
                   kernel_regularizer=regularizers.l2(0.01)))
  #model.add(LeakyReLU(0.1))
  model.add(Activation('relu'))

  model.add(Conv1D(filters=64,kernel_size=5,padding="valid",kernel_regularizer=regularizers.l2(0.01)))
  #model.add(LeakyReLU(0.1))
  model.add(Activation('relu'))

  model.add(MaxPooling1D(3,padding="valid"))

  model.add(Conv1D(filters=128,kernel_size=5,padding="valid",kernel_regularizer=regularizers.l2(0.01)))
  #model.add(LeakyReLU(0.1))
  model.add(Activation('relu'))

  model.add(Conv1D(filters=128,kernel_size=5,padding="valid",kernel_regularizer=regularizers.l2(0.01)))
  #model.add(LeakyReLU(0.1))
  model.add(Activation('relu'))

  #model.add(MaxPooling1D(3,padding="valid"))

  model.add(GlobalAveragePooling1D())
  model.add(Dropout(0.5))

  #model.add(Dense(100,kernel_regularizer=regularizers.l2(0.01)))
  #model.add(LeakyReLU(0.1))
  #model.add(Activation('relu'))
  model.add(Dense(157,activation='sigmoid',kernel_regularizer=regularizers.l2(0.01)))


  return model


BATCH_SIZE = 32
EPOCHS = 10
STEPS_PER_EPOCH = 100

model = conv1d_model(157)
print(model.summary())

adam = optimizers.Adam(lr=0.0005)

model.compile(optimizer=adam,loss='categorical_crossentropy', metrics=['accuracy'])


tensorboard = TensorBoard(log_dir='./logs', histogram_freq=0, batch_size=BATCH_SIZE, write_graph=True, write_grads=False, write_images=False, embeddings_freq=0, embeddings_layer_names=None, embeddings_metadata=None, embeddings_data=None, update_freq='epoch')

history = model.fit_generator(generator=batch_generator(BATCH_SIZE),
                              epochs=EPOCHS,
                              verbose=1,
                              steps_per_epoch =STEPS_PER_EPOCH,
                              shuffle=False,
                              callbacks=[tensorboard])
