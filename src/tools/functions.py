import pandas as pd
import numpy as np
import pickle
import re
from sklearn.feature_extraction.text import TfidfVectorizer


def load_pickle(pkl):
  df =pickle.load(pkl)
  return df

def write_to_pickle(data, filename):
  with open(filename, 'wb') as file:
    pickle.dump(data,file)



def df_from_csv(csv):
  df = pd.read_csv(csv)
  return df

def create_vectorizer():
  return TfidfVectorizer(analyzer= 'word', token_pattern = '[^\|]+(?=\|)|[^\|]+')

def create_tfidf_matrix(vectorizer,corpus):
  return vectorizer.fit_transform(corpus.properties)

def get_top25(vectorizer, tfidf_matrix,corpus,query):
  result =  vectorizer.transform(query)
  score = cosine_similarity(result,tfidf_matrix).flatten()
  doc_list = list(map(lambda x: corpus.iloc[x].item_id, score.argsort()[:-26:-1]))
  return doc_list

def query_ranked(vectorizer, tfidf_matrix,corpus,query):
  result =  vectorizer.transform(query)
  score = cosine_similarity(result,tfidf_matrix).flatten()
  return list(map(lambda x: corpus.iloc[x].item_id, score.argsort()[:-5000:-1]))


def get_submission_target(df):
  mask = df["reference"].isnull() & (df["action_type"] == "clickout item")
  df_out = df[mask]
  return df_out


def get_item_properties(item_id,metadata):
  try:
    item_id = int(float(item_id))
    properties = metadata.loc[metadata.item_id == item_id].properties.values[0]
    item_properties = re.findall('[^\|]+(?=\|)|[^\|]+', properties)
  except (IndexError,ValueError) as e:
    item_properties = []
  return item_properties

def filter_selections_to_array(selection):
  filters = []
  if isinstance(selection,str):
    filters = re.findall('[^\|]+(?=\|)|[^\|]+', selection)
  return filters

def get_impressions(clickout_event):
  impressions = re.findall('[^\|]+(?=\|)|[^\|]+', clickout_event.impressions)
  return impressions

def create_observation(arr, feature_names):
  observation_array = np.full(157,0.1)
  for i in arr:
    if i.lower() in feature_names:
      observation_array[feature_names.index(i.lower())] = 0.9
  return observation_array
