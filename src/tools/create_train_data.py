import gc
import sys
import pandas as pd
import numpy as np
import functions as f

sys.path.insert(0,"../../../")

vectorizer = f.create_vectorizer()

train_data = f.df_from_csv("../../data/test.csv")
mask = train_data.action_type.isin(['interaction item image'
                                    ,'interaction item info'
                                    ,'interaction item rating'
                                    ,'interaction item deals'
                                    ,'filter selection'
                                    ,'clickout item'])
train_data = train_data[mask]
gc.collect()
metadata = f.df_from_csv("../../data/item_metadata.csv")

user_ids = train_data.user_id.unique()

vectorizer.fit_transform(metadata.properties)

feature_names = vectorizer.get_feature_names()


df = pd.DataFrame(columns=['label','observation'])
user_count = len(user_ids)
count = 0
for i in user_ids:
  session =  train_data.loc[train_data.user_id == i]
  observation_array = []
  for s in session.index:
    current = session.loc[s]
    if current.action_type == "clickout item":
      #create label for this session
      label = f.create_observation(f.get_item_properties(current.reference,metadata),feature_names)
      #label = np.ones(157)
      impressions = f.get_impressions(current)
      for imp in impressions:
        arr= f.create_observation(f.get_item_properties(imp,metadata),feature_names)
        observation_array.append(arr)

        df1 = pd.DataFrame([[np.array(label),np.array(observation_array)]]
                         ,columns=['label','observation'])
      df= pd.concat([df1,df])
      df1 = []
      label= []
      impressions = []
      observation_array = []
      gc.collect()
    else:
      arr = np.zeros(157)
      #accumulate session obs
      if current.action_type == "filter selection":
        arr = f.create_observation(f.filter_selections_to_array(current.current_filters),feature_names)

      else:
        arr = f.create_observation(f.get_item_properties(current.reference,metadata),feature_names)
      observation_array.append(arr)

  print("written nr: ",count, "of: ",user_count)
  count += 1
f.write_to_pickle(df,"train_data.pkl")
